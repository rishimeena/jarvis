#!/usr/bin/python
import os
import speech_recognition as sr
 
print sr

def openApp (appName):
  startApp = "open -a "+appName
  os.system(startApp)
  return;

def textToSpeech(text):
  os.system("echo "+text)
  os.system("say "+text)
  return;

def text(text):
  os.system("echo "+text)
  return;

def closeApp (appName):
  closeApp = "osascript -e 'quit app "+'"'+appName+'"'+"'"
  os.system(closeApp);
  return;


# Record Audio
def listenStart():
  r = sr.Recognizer()
  with sr.Microphone() as source:
      print "listening..."
      audio = r.listen(source)

  # Speech recognition using Google Speech Recognition
  try:
      # for testing purposes, we're just using the default API key
      # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
      # instead of `r.recognize_google(audio)`
      print audio
      audioAsText = r.recognize_google(audio)
      text("You said: " + audioAsText)

      arr = audioAsText.split()
      if audioAsText.find('open') > -1:
        appName = arr[audioAsText.find('open') + 1]
        textToSpeech('ok sir, starting '+appName)
        openApp(appName)
        listenStart()

      if audioAsText.find('close') > -1:
        appName = arr[audioAsText.find('close') + 1]
        textToSpeech('ok sir, closing '+appName)
        closeApp(appName)
        listenStart()

      if audioAsText:
        listenStart() 


  except sr.UnknownValueError:
      text("Sorry Rishi, I didnt understand it")
      listenStart()
  except sr.RequestError as e:
      text("Sorry Rishi, I cant run your command")
      listenStart()


textToSpeech("Hi Rishi")
listenStart()




# print "Hello, Python!"

# appName = 'skype'

# Open a application
# startApp = "open -a "+appName
# os.system(startApp)

# Open a application
# closeApp = "osascript -e 'quit app "+'"'+appName+'"'+"'"
# os.system(closeApp);


# a = "osascript -e 'tell application "+'"iTunes" '+ "to next track'"
# print a
# os.system(a);



